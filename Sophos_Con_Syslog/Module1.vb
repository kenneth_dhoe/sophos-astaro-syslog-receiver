﻿Imports System.Net
Imports System.Text
Imports System.Net.Sockets

Module Module1
    Dim i As Integer = Nothing
    Dim int_dropped As Integer = Nothing
    Dim int_accepted As Integer = Nothing
    Dim int_logged As Integer = Nothing
    Dim int_alert As Integer = Nothing
    Dim int_notfw As Integer = Nothing
    Dim port As Integer
    Public ipeRemoteIpEndPoint As New IPEndPoint(IPAddress.Any, 0)

    Public sDataRecieve As String
    Public bBytesRecieved() As Byte

    Sub Main()
        Console.Title = ("Sophos Syslog Receiver, By Kenneth")


        Console.WindowWidth = 70
        Console.WindowHeight = 12

        Console.BufferHeight = 12
        Console.BufferWidth = 70

        Do
            Dim intInput As Integer = 0
            Dim c_width As Integer = Console.WindowWidth
            Console.Clear()
            'Console.BackgroundColor = ConsoleColor.Blue
            Console.ForegroundColor = ConsoleColor.Cyan
            Console.WriteLine()
            Console.Write("Please enter the port where the application should listen on: ")

            If Integer.TryParse(Console.ReadLine, port) Then
                If port > 65535 Then
                    Console.WriteLine("Please enter a valid port number!")
                Else
                    Console.Clear()
                    Console.WriteLine()
                    Console.WriteLine("+-----------------+")
                    Console.Write(String.Format("|{0,-10}", "    Main Menu    |") & vbNewLine)
                    Console.WriteLine("+----+------------+")
                    Console.Write(String.Format("|{0,-4}|{1,-4}{2,4}", "  1", " Firewall", "|") & vbNewLine)
                    Console.Write(String.Format("|{0,-4}|{1,-4}{2,9}", "  2", " IPS", "|") & vbNewLine)
                    Console.Write(String.Format("|{0,-4}|{1,-4}{2,2}", "  3", " Http Proxy", "|") & vbNewLine)
                    Console.Write(String.Format("|{0,-4}|{1,-4}{2,7}", "  4", " Other", "|") & vbNewLine)
                    Console.Write(String.Format("|{0,-4}|{1,-4}{2,7}", "  5", " Debug", "|") & vbNewLine)
                    Console.Write(String.Format("|{0,-4}|{1,-4}{2,7}", "  6", " TEST ", "|") & vbNewLine)
                    Console.WriteLine("+----+------------+")
                    Console.WriteLine()
                    Console.Write("Please enter a number from 1 - 5: ")


                    If Integer.TryParse(Console.ReadLine(), intInput) Then
                        Select Case intInput
                            Case 1
                                Firewall_Log()
                            Case 2
                                IPS_Log()
                            Case 3
                                Proxy_log()

                            Case 4
                                Other()
                            Case 5
                                Debug()
                            Case 6
                                spy_list()
                            Case Else
                                Console.WriteLine("Please select from 1 to 5")
                        End Select
                    Else
                        Console.WriteLine("Please select from 1 to 5")
                    End If

                End If


            Else
                Console.WriteLine("Please enter a valid port number!")
            End If

          
        Loop


    End Sub



    Public Sub Proxy_log()
        Dim udpcUDPClient As New UdpClient(port)
        Console.BackgroundColor = ConsoleColor.Black
        Console.ForegroundColor = ConsoleColor.White
        Console.Clear()


        AddHandler AppDomain.CurrentDomain.ProcessExit, AddressOf MyApp_ProcessExit


        ' Do some time-consuming work on this thread.

        Console.WindowWidth = 110
        Console.WindowHeight = 50

        Console.BufferHeight = 50
        Console.BufferWidth = 110


        Console.WriteLine("Loading Syslog Sophos Proxy...")
        Console.BackgroundColor = ConsoleColor.Blue
        Console.SetCursorPosition(0, 0)
        Console.ForegroundColor = ConsoleColor.Red
        Console.Write(String.Format("{0,-26}{1,10}", " BLOCKED: ", int_dropped))
        Console.ForegroundColor = ConsoleColor.Green
        Console.Write(String.Format("{0,-26}{1,10}", " PASSED: ", int_accepted))
        Console.ForegroundColor = ConsoleColor.Yellow
        Console.Write(String.Format("{0,-26}{1,11}", " WARNINGS: ", int_logged) & vbNewLine)
        Console.ForegroundColor = ConsoleColor.Cyan

        Console.Write(String.Format("{0,-17}{1,-17}{2,-17}{3,-39}{4,-11}{5,-8}", " TIME", "SOURCE-IP", "DEST-IP", "URL", "ACTION", "METHOD") & vbNewLine)
        Console.ForegroundColor = ConsoleColor.White
        While True

            bBytesRecieved = udpcUDPClient.Receive(ipeRemoteIpEndPoint)
            sDataRecieve = Encoding.ASCII.GetString(bBytesRecieved)
            'sFromIP = ipeRemoteIpEndPoint.Address.ToString

            'FillLog(sDataRecieve, sFromIP)

            sDataRecieve = sDataRecieve.Replace(vbCrLf, "")
            sDataRecieve = sDataRecieve.Replace(vbNewLine, "")
            sDataRecieve = sDataRecieve.Replace(vbNullChar, "")
            sDataRecieve = sDataRecieve.Replace(vbCr, "")
            sDataRecieve = sDataRecieve.Replace(vbLf, "")
            sDataRecieve = Trim(sDataRecieve)


            Dim value_srcip As String = GetStringBetween(substring(sDataRecieve, "srcip"))
            Dim value_dstip As String = GetStringBetween(substring(sDataRecieve, "dstip"))
            Dim value_action As String = GetStringBetween(substring(sDataRecieve, "action"))
            Dim value_url As String = GetStringBetween(substring(sDataRecieve, "url"))
            Dim value_method As String = GetStringBetween(substring(sDataRecieve, "method"))
            Dim value_system As String = GetStringBetween(substring(sDataRecieve, "sys"))

            If value_system = "SecureWeb" And Not sDataRecieve.Contains("nil") And Not sDataRecieve.Contains("connect_") Then

                Console.Title = "latest packet: " & Now & " " & "Sophos syslog Receiver, Proxy log"
                Dim Height_ As Integer = Console.WindowHeight - 3


                Console.BufferHeight += 1



                If value_action = "block" Then
                    int_dropped += 1
                ElseIf value_action = "pass" Then
                    int_accepted += 1
                ElseIf value_action = "log" Then
                    int_logged += 1
                End If

                i += 1

                If i > Height_ Then
                    Console.BackgroundColor = ConsoleColor.Blue
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.SetCursorPosition(0, i - Height_)
                    Console.ForegroundColor = ConsoleColor.Red
                    Console.Write(String.Format("{0,-26}{1,10}", " BLOCKED: ", int_dropped))
                    Console.ForegroundColor = ConsoleColor.Green
                    Console.Write(String.Format("{0,-26}{1,10}", " PASSED: ", int_accepted))
                    Console.ForegroundColor = ConsoleColor.Yellow
                    Console.Write(String.Format("{0,-26}{1,11}", " WARNINGS: ", int_logged) & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.Write(String.Format("{0,-17}{1,-17}{2,-17}{3,-39}{4,-11}{5,-8}", " TIME", "SOURCE-IP", "DEST-IP", "URL", "ACTION", "METHOD") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.White
                    Console.SetCursorPosition(0, i + 1)
                Else
                    Console.BackgroundColor = ConsoleColor.Blue
                    Console.SetCursorPosition(0, 0)
                    Console.ForegroundColor = ConsoleColor.Red
                    Console.Write(String.Format("{0,-26}{1,10}", " BLOCKED: ", int_dropped))
                    Console.ForegroundColor = ConsoleColor.Green
                    Console.Write(String.Format("{0,-26}{1,10}", " PASSED: ", int_accepted))
                    Console.ForegroundColor = ConsoleColor.Yellow
                    Console.Write(String.Format("{0,-26}{1,11}", " WARNINGS: ", int_logged) & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.Write(String.Format("{0,-17}{1,-17}{2,-17}{3,-39}{4,-11}{5,-8}", " TIME", "SOURCE-IP", "DEST-IP", "URL", "ACTION", "METHOD") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.White
                    Console.SetCursorPosition(0, i + 1)
                End If



                'Console.WriteLine(Now & vbTab & value_srcip & vbTab & value_srcport & vbTab & "  ->  " & vbTab & value_dstip & vbTab & value_dstport & vbTab & value_action)
                If value_action = "block" Then
                    Console.ForegroundColor = ConsoleColor.Red
                ElseIf value_action = "pass" Then
                    Console.ForegroundColor = ConsoleColor.Green
                ElseIf value_action = "warn" Then
                    Console.ForegroundColor = ConsoleColor.Yellow
                Else
                    Console.ForegroundColor = ConsoleColor.Blue
                End If

                If value_url.Length < 38 Then
                    value_url = value_url
                ElseIf value_url.Length > 38 Then
                    value_url = value_url.Substring(0, 38)
                End If

                Console.BackgroundColor = ConsoleColor.Black

                Console.WriteLine(String.Format("{0,-17}{1,-17}{2,-17}{3,-39}{4,-11}{5,-8}", " " & Now.ToLongTimeString, value_srcip, value_dstip, value_url, value_action, value_method))
                'RichTextBox1.AppendText(sDataRecieve)
                sDataRecieve = Nothing


            Else
                int_notfw += 1
                sDataRecieve = Nothing
            End If

        End While

    End Sub

    Public Sub max_Test()
       
    End Sub



    Public Sub spy_list()
       
    End Sub




    Public Sub Firewall_Log()
      
        Dim udpcUDPClient As New UdpClient(port)
        Console.BackgroundColor = ConsoleColor.Black
        Console.ForegroundColor = ConsoleColor.White
        Console.Clear()


        AddHandler AppDomain.CurrentDomain.ProcessExit, AddressOf MyApp_ProcessExit

        ' Do some time-consuming work on this thread.

        Console.WindowWidth = 110
        Console.WindowHeight = 50

        Console.BufferHeight = 50
        Console.BufferWidth = 110


        Console.WriteLine("Loading Syslog Sophos Firewall...")
        Console.BackgroundColor = ConsoleColor.Blue
        Console.SetCursorPosition(0, 0)
        Console.ForegroundColor = ConsoleColor.Red
        Console.Write(String.Format("{0,-26}{1,10}", " DROPPED: ", int_dropped))
        Console.ForegroundColor = ConsoleColor.Green
        Console.Write(String.Format("{0,-26}{1,10}", " PERMITTED: ", int_accepted))
        Console.ForegroundColor = ConsoleColor.Yellow
        Console.Write(String.Format("{0,-26}{1,11}", " LOGGED: ", int_logged) & vbNewLine)
        Console.ForegroundColor = ConsoleColor.Cyan

        Console.Write(String.Format("{0,-17}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-10}{7,-7}{8,-9}", " TIME", "SOURCE-IP", "SOURCE-PORT", "DEST-IP", "DEST-PORT", "ACTION", "TCP-FLAG", "FWRULE", "DIR") & vbNewLine)
        Console.ForegroundColor = ConsoleColor.White
        While True

            bBytesRecieved = udpcUDPClient.Receive(ipeRemoteIpEndPoint)
            sDataRecieve = Encoding.ASCII.GetString(bBytesRecieved)
            'sFromIP = ipeRemoteIpEndPoint.Address.ToString

            'FillLog(sDataRecieve, sFromIP)

            sDataRecieve = sDataRecieve.Replace(vbCrLf, "")
            sDataRecieve = sDataRecieve.Replace("ICMP flood", "ICMP_F")
            sDataRecieve = sDataRecieve.Replace(vbNewLine, "")
            sDataRecieve = sDataRecieve.Replace(vbNullChar, "")
            sDataRecieve = sDataRecieve.Replace(vbCr, "")
            sDataRecieve = sDataRecieve.Replace(vbLf, "")
            sDataRecieve = Trim(sDataRecieve)



            Dim value_srcip As String = GetStringBetween(substring(sDataRecieve, "srcip"))
            Dim value_dstip As String = GetStringBetween(substring(sDataRecieve, "dstip"))
            Dim value_srcport As String = GetStringBetween(substring(sDataRecieve, "srcport"))
            Dim value_dstport As String = GetStringBetween(substring(sDataRecieve, "dstport"))
            Dim value_action As String = GetStringBetween(substring(sDataRecieve, "action"))
            Dim value_tcpflag As String = GetStringBetween(substring(sDataRecieve, "tcpflags"))
            Dim value_fwrule As String = GetStringBetween(substring(sDataRecieve, "fwrule"))
            Dim value_system As String = GetStringBetween(substring(sDataRecieve, "sys"))
            Dim value_sub As String = GetStringBetween(substring(sDataRecieve, "sub"))
            Dim value_dir As String = Nothing

            If value_system = "SecureNet" Then

                Console.Title = "latest packet: " & Now & " " & "Sophos syslog Receiver, Firewall log"
                Dim Height_ As Integer = Console.WindowHeight - 3

                If value_sub = "ips" Then
                    value_action = "drop"

                End If

                If i = Int16.MaxValue - 1 Then
                    i = 0
                    Console.Clear()
                    Console.SetCursorPosition(0, 0)
                    Console.BackgroundColor = ConsoleColor.Blue
                    Console.SetCursorPosition(0, 0)
                    Console.ForegroundColor = ConsoleColor.Red
                    Console.Write(String.Format("{0,-26}{1,10}", " DROPPED: ", int_dropped))
                    Console.ForegroundColor = ConsoleColor.Green
                    Console.Write(String.Format("{0,-26}{1,10}", " PERMITTED: ", int_accepted))
                    Console.ForegroundColor = ConsoleColor.Yellow
                    Console.Write(String.Format("{0,-26}{1,11}", " LOGGED: ", int_logged) & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.Cyan

                    Console.Write(String.Format("{0,-17}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-10}{7,-7}{8,-9}", " TIME", "SOURCE-IP", "SOURCE-PORT", "DEST-IP", "DEST-PORT", "ACTION", "TCP-FLAG", "FWRULE", "DIR") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.White
                End If

                i += 1
                If value_action = "drop" Or value_action = "ICMP_F" Then
                    int_dropped += 1
                ElseIf value_action = "accept" Then
                    int_accepted += 1
                ElseIf value_action = "log" Then
                    int_logged += 1
                End If

                If i > Height_ Then
                    Console.BufferHeight += 1
                    Console.BackgroundColor = ConsoleColor.Blue
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.SetCursorPosition(0, i - Height_) ' << thats being a bitch xD
                    Console.ForegroundColor = ConsoleColor.Red
                    Console.Write(String.Format("{0,-26}{1,10}", " DROPPED: ", int_dropped))
                    Console.ForegroundColor = ConsoleColor.Green
                    Console.Write(String.Format("{0,-26}{1,10}", " PERMITTED: ", int_accepted))
                    Console.ForegroundColor = ConsoleColor.Yellow
                    Console.Write(String.Format("{0,-26}{1,11}", " LOGGED: ", int_logged) & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.Write(String.Format("{0,-17}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-10}{7,-7}{8,-9}", " TIME", "SOURCE-IP", "SOURCE-PORT", "DEST-IP", "DEST-PORT", "ACTION", "TCP-FLAG", "FWRULE", "DIR") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.White
                    Console.SetCursorPosition(0, i + 1) ' god damned stupid poosition xD waar zie je de error waarde ? xD
                
                Else
                    Console.BackgroundColor = ConsoleColor.Blue
                    Console.SetCursorPosition(0, 0)
                    Console.ForegroundColor = ConsoleColor.Red
                    Console.Write(String.Format("{0,-26}{1,10}", " DROPPED: ", int_dropped))
                    Console.ForegroundColor = ConsoleColor.Green
                    Console.Write(String.Format("{0,-26}{1,10}", " PERMITTED: ", int_accepted))
                    Console.ForegroundColor = ConsoleColor.Yellow
                    Console.Write(String.Format("{0,-26}{1,11}", " LOGGED: ", int_logged) & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.Write(String.Format("{0,-17}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-10}{7,-7}{8,-9}", " TIME", "SOURCE-IP", "SOURCE-PORT", "DEST-IP", "DEST-PORT", "ACTION", "TCP-FLAG", "FWRULE", "DIR") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.White
                    Console.SetCursorPosition(0, i + 1)
                End If


                If value_action = "drop" Or value_action = "ICMP_F" Then
                    Console.ForegroundColor = ConsoleColor.Red
                ElseIf value_action = "accept" Then
                    Console.ForegroundColor = ConsoleColor.Green
                ElseIf value_action = "log" Then
                    Console.ForegroundColor = ConsoleColor.Yellow
                Else
                    Console.ForegroundColor = ConsoleColor.Blue
                End If


                If value_srcip.StartsWith("192.168.") Or value_srcip.StartsWith("172.16") Or value_srcip.StartsWith("10.") = True Then
                    value_dir = "OUT"
                Else
                    value_dir = "IN"
                End If


                Dim line As String = String.Format("{0,-17}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-10}{7,-7}{8,-5}", " " & Now.ToLongTimeString, value_srcip, value_srcport, value_dstip, value_dstport, value_action, value_tcpflag, value_fwrule, value_dir)
                Console.BackgroundColor = ConsoleColor.Black

                Console.WriteLine(line)
                'RichTextBox1.AppendText(sDataRecieve)
                sDataRecieve = Nothing

            Else
                int_notfw += 1
                sDataRecieve = Nothing
            End If

        End While
    End Sub

    Public Sub IPS_Log()
        Dim udpcUDPClient As New UdpClient(port)
        Console.BackgroundColor = ConsoleColor.Black
        Console.ForegroundColor = ConsoleColor.White
        Console.Clear()


        AddHandler AppDomain.CurrentDomain.ProcessExit, AddressOf MyApp_ProcessExit

        ' Do some time-consuming work on this thread.

        Console.WindowWidth = 150
        Console.WindowHeight = 50

        Console.BufferHeight = 50
        Console.BufferWidth = 150


        Console.WriteLine("Loading Syslog Sophos Firewall...")
        Console.BackgroundColor = ConsoleColor.Blue
        Console.SetCursorPosition(0, 0)
        Console.ForegroundColor = ConsoleColor.Yellow
        Console.Write(String.Format("{0,-26}{1,10}", " ALERT: ", int_alert))
        Console.ForegroundColor = ConsoleColor.Red
        Console.Write(String.Format("{0,-26}{1,10}", " DROP: ", int_dropped))
        Console.Write(String.Format("{0,-77}", "") & vbNewLine)
        Console.ForegroundColor = ConsoleColor.Cyan
        Console.Write(String.Format("{0,-10}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-64}{7,-9}", " TIME", "SOURCE-IP", "SOURCE-PORT", "DEST-IP", "DEST-PORT", "ACTION", "REASON", "DIR") & vbNewLine)
        Console.ForegroundColor = ConsoleColor.White

        While True

            bBytesRecieved = udpcUDPClient.Receive(ipeRemoteIpEndPoint)
            sDataRecieve = Encoding.ASCII.GetString(bBytesRecieved)
            'sFromIP = ipeRemoteIpEndPoint.Address.ToString

            'FillLog(sDataRecieve, sFromIP)

            sDataRecieve = sDataRecieve.Replace(vbCrLf, "")
            sDataRecieve = sDataRecieve.Replace("Packet dropped", "Packet_dropped")
            sDataRecieve = sDataRecieve.Replace("ICMP flood", "ICMP_F")
            sDataRecieve = sDataRecieve.Replace(vbNewLine, "")
            sDataRecieve = sDataRecieve.Replace(vbNullChar, "")
            sDataRecieve = sDataRecieve.Replace(vbCr, "")
            sDataRecieve = sDataRecieve.Replace(vbLf, "")
            sDataRecieve = Trim(sDataRecieve)


            Dim value_srcip As String = GetStringBetween(substring(sDataRecieve, "srcip"))
            Dim value_dstip As String = GetStringBetween(substring(sDataRecieve, "dstip"))
            Dim value_srcport As String = GetStringBetween(substring(sDataRecieve, "srcport"))
            Dim value_dstport As String = GetStringBetween(substring(sDataRecieve, "dstport"))
            Dim value_action As String = GetStringBetween(substring(sDataRecieve, "action"))
            Dim value_tcpflag As String = GetStringBetween(substring(sDataRecieve, "tcpflags"))
            Dim value_fwrule As String = GetStringBetween(substring(sDataRecieve, "fwrule"))
            Dim value_system As String = GetStringBetween(substring(sDataRecieve, "sys"))
            Dim value_sub As String = GetStringBetween(substring(sDataRecieve, "sub"))
            Dim value_reason As String = GetStringBetween(substring(sDataRecieve, "reason"))
            Dim value_name As String = GetStringBetween(substring(sDataRecieve, "name"))
            Dim value_dir As String = Nothing

            If value_system = "SecureNet" And value_sub = "ips" Then

                Console.Title = "latest packet: " & Now & " " & "Sophos syslog Receiver, IPS log"
                Dim Height_ As Integer = Console.WindowHeight - 3

                i += 1


                If i = Int16.MaxValue - 1 Then
                    i = 0
                    Console.Clear()
                    Console.SetCursorPosition(0, 0)
                    Console.BackgroundColor = ConsoleColor.Blue
                    Console.SetCursorPosition(0, 0)
                    Console.ForegroundColor = ConsoleColor.Yellow
                    Console.Write(String.Format("{0,-26}{1,10}", " ALERT: ", int_alert))
                    Console.ForegroundColor = ConsoleColor.Red
                    Console.Write(String.Format("{0,-26}{1,10}", " DROP: ", int_dropped))
                    Console.Write(String.Format("{0,-77}", "") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.Write(String.Format("{0,-10}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-64}{7,-9}", " TIME", "SOURCE-IP", "SOURCE-PORT", "DEST-IP", "DEST-PORT", "ACTION", "REASON", "DIR") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.White

                End If

                If value_reason = Nothing Or value_reason = "" Then
                    value_reason = value_name
                    value_action = "drop"
                End If

                If value_action = "portscan" Then
                    value_reason = value_name
                    value_action = "drop"
                End If

                If value_srcport = "0" Or value_dstport = "0" Then
                    value_srcport = "---"
                    value_dstport = "---"
                End If

                If value_action = "drop" Then
                    int_dropped += 1
                ElseIf value_action = "alert" Then
                    int_alert += 1
                End If

                If i > Height_ Then
                    Console.BufferHeight += 1
                    Console.BackgroundColor = ConsoleColor.Blue
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.SetCursorPosition(0, i - Height_)
                    Console.ForegroundColor = ConsoleColor.Yellow
                    Console.Write(String.Format("{0,-26}{1,10}", " ALERT: ", int_alert))
                    Console.ForegroundColor = ConsoleColor.Red
                    Console.Write(String.Format("{0,-26}{1,10}", " DROP: ", int_dropped))
                    Console.Write(String.Format("{0,-77}", "") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.Write(String.Format("{0,-10}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-64}{7,-9}", " TIME", "SOURCE-IP", "SOURCE-PORT", "DEST-IP", "DEST-PORT", "ACTION", "REASON", "DIR") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.White
                    Console.SetCursorPosition(0, i + 1)
                Else
                    Console.BackgroundColor = ConsoleColor.Blue
                    Console.SetCursorPosition(0, 0)
                    Console.ForegroundColor = ConsoleColor.Yellow
                    Console.Write(String.Format("{0,-26}{1,10}", " ALERT: ", int_alert))
                    Console.ForegroundColor = ConsoleColor.Red
                    Console.Write(String.Format("{0,-26}{1,10}", " DROP: ", int_dropped))
                    Console.Write(String.Format("{0,-77}", "") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.Cyan
                    Console.Write(String.Format("{0,-10}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-64}{7,-9}", " TIME", "SOURCE-IP", "SOURCE-PORT", "DEST-IP", "DEST-PORT", "ACTION", "REASON", "DIR") & vbNewLine)
                    Console.ForegroundColor = ConsoleColor.White
                    Console.SetCursorPosition(0, i + 1)
                End If


                If value_action = "drop" Then
                    Console.ForegroundColor = ConsoleColor.Red
                ElseIf value_action = "alert" Then
                    Console.ForegroundColor = ConsoleColor.Yellow
                Else
                    Console.ForegroundColor = ConsoleColor.Blue
                End If


                If value_srcip.StartsWith("192.168.") Or value_srcip.StartsWith("172.16") Or value_srcip.StartsWith("10.") = True Then
                    value_dir = "OUT"
                Else
                    value_dir = "IN"
                End If

                Console.BackgroundColor = ConsoleColor.Black
                Console.WriteLine(String.Format("{0,-10}{1,-17}{2,-13}{3,-17}{4,-11}{5,-8}{6,-64}{7,-9}", " " & Now.ToLongTimeString, value_srcip, value_srcport, value_dstip, value_dstport, value_action, value_reason, value_dir))
                'RichTextBox1.AppendText(sDataRecieve)
                sDataRecieve = Nothing

            Else
                int_notfw += 1
                sDataRecieve = Nothing
            End If

        End While
    End Sub

    Public Sub Other()
        Dim udpcUDPClient As New UdpClient(port)
        Console.BackgroundColor = ConsoleColor.Black
        Console.ForegroundColor = ConsoleColor.White
        Console.Clear()


        AddHandler AppDomain.CurrentDomain.ProcessExit, AddressOf MyApp_ProcessExit

        ' Do some time-consuming work on this thread.
        Console.WindowWidth = Console.LargestWindowWidth - 10
        Console.WindowHeight = Console.LargestWindowHeight - 10

        Console.SetWindowPosition(0, 0)

        Console.WriteLine("Waiting for first message...")
        While True

            bBytesRecieved = udpcUDPClient.Receive(ipeRemoteIpEndPoint)
            sDataRecieve = Encoding.ASCII.GetString(bBytesRecieved)
            'sFromIP = ipeRemoteIpEndPoint.Address.ToString

            'FillLog(sDataRecieve, sFromIP)

            sDataRecieve = sDataRecieve.Replace(vbCrLf, "")
            sDataRecieve = sDataRecieve.Replace("ICMP flood", "ICMP_F")
            sDataRecieve = sDataRecieve.Replace(vbNewLine, "")
            sDataRecieve = sDataRecieve.Replace(vbNullChar, "")
            sDataRecieve = sDataRecieve.Replace(vbEmpty, "")
            sDataRecieve = sDataRecieve.Replace(vbCr, "")
            sDataRecieve = sDataRecieve.Replace(vbLf, "")
            sDataRecieve = Trim(sDataRecieve)



            Console.Title = "latest packet: " & Now & " " & "Sophos syslog Receiver, All Syslog"
            Dim Height_ As Integer = Console.WindowHeight - 3


            Console.BufferHeight += 1

            Dim value_system As String = GetStringBetween(substring(sDataRecieve, "sys"))


            i += 1

            If i = Int16.MaxValue - 1 Then
                i = 0
                Console.Clear()
                Console.SetCursorPosition(0, 0)

            End If

            Console.BackgroundColor = ConsoleColor.Black
            If value_system = "SecureNet" Or value_system = "SecureWeb" Or sDataRecieve.Contains("SecureNet") Or sDataRecieve.Contains("SecureWeb") Then
                sDataRecieve = Nothing
            Else
                If sDataRecieve.Length > Console.BufferWidth Then
                    Console.BufferWidth = sDataRecieve.Length
                End If

                Console.WriteLine(sDataRecieve)
            End If


            'RichTextBox1.AppendText(sDataRecieve)
            sDataRecieve = Nothing


        End While
    End Sub

    Public Sub Debug()
        Dim udpcUDPClient As New UdpClient(port)
        Console.BackgroundColor = ConsoleColor.Black
        Console.ForegroundColor = ConsoleColor.White
        Console.Clear()


        AddHandler AppDomain.CurrentDomain.ProcessExit, AddressOf MyApp_ProcessExit

        ' Do some time-consuming work on this thread.
        Console.WindowWidth = Console.LargestWindowWidth - 10
        Console.WindowHeight = Console.LargestWindowHeight - 10

        Console.SetWindowPosition(0, 0)

        Console.WriteLine("Waiting for first message...")
        While True

            bBytesRecieved = udpcUDPClient.Receive(ipeRemoteIpEndPoint)
            sDataRecieve = Encoding.ASCII.GetString(bBytesRecieved)
            'sFromIP = ipeRemoteIpEndPoint.Address.ToString

            'FillLog(sDataRecieve, sFromIP)

            sDataRecieve = sDataRecieve.Replace(vbCrLf, "")
            sDataRecieve = sDataRecieve.Replace("ICMP flood", "ICMP_F")
            sDataRecieve = sDataRecieve.Replace(vbNullChar, "")
            sDataRecieve = sDataRecieve.Replace(vbEmpty, "")
            sDataRecieve = sDataRecieve.Replace(vbCr, "")
            sDataRecieve = sDataRecieve.Replace(vbLf, "")
            sDataRecieve = Trim(sDataRecieve)



            Console.Title = "latest packet: " & Now & " " & "Sophos syslog Receiver, All Syslog"
            Dim Height_ As Integer = Console.WindowHeight - 3


            Console.BufferHeight += 1

            Dim value_system As String = GetStringBetween(substring(sDataRecieve, "sys"))


            i += 1
            If i = Int16.MaxValue - 1 Then
                i = 0
                Console.Clear()
                Console.SetCursorPosition(0, 0)

            End If
            Console.BackgroundColor = ConsoleColor.Black
            
                If sDataRecieve.Length > Console.BufferWidth Then
                    Console.BufferWidth = sDataRecieve.Length
                End If

                Console.WriteLine(sDataRecieve)



            'RichTextBox1.AppendText(sDataRecieve)
            sDataRecieve = Nothing


        End While
    End Sub

    Private Sub MyApp_ProcessExit(sender As Object, e As EventArgs)
        Console.WriteLine("Closing Syslog...")
        End
    End Sub

    Public Function substring(string_ As String, searhfor As String)
        Try
            Dim firstchar As Integer = Nothing
            Dim _substring As String = Nothing
            firstchar = string_.IndexOf(searhfor)
            _substring = string_.Substring(firstchar)

            Return _substring
        Catch
            Return "---"
            Exit Function

        End Try

    End Function

    Public Function GetStringBetween(ByVal InputText As String)

        Dim startPos As Integer
        Dim endPos As Integer
        Dim lenStart As Integer
        Dim starttext As String = """"
        Dim endtext As String = """"

        startPos = InputText.IndexOf(starttext, StringComparison.CurrentCultureIgnoreCase)
        If startPos >= 0 Then
            lenStart = startPos + starttext.Length
            endPos = InputText.IndexOf(endtext, lenStart, StringComparison.CurrentCultureIgnoreCase)
            If endPos >= 0 Then
                Return InputText.Substring(lenStart, endPos - lenStart)
            End If
        End If
        Return "---"
    End Function
End Module
